﻿using UnityEngine;
using System.Collections;

public class Draw : MonoBehaviour {

    private Vector3 mousePos;
    TextMesh ugol_bar;
    GameObject kometa;
    GameObject hwost;
    public GameObject [] uglu;
    int count_uglu, cur_ugol = -1, last_ugol = -1, vector = 0;
        public int point = 0;
    GameController game;
    float time, round_time = 3;

    public bool draw = false, On = false;
	// Use this for initialization
	void Start () {
        mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
       kometa = GameObject.Find("kometa");
       hwost = GameObject.Find("hw");
       ugol_bar = GameObject.Find("ugol_bar").GetComponent<TextMesh>();
      
        game = GameObject.FindObjectOfType<GameController>();
       // Debug.Log("count uglu " + count_uglu + "  ug = " + uglu[0].name);
    }

    public void set_figure()
    {
        
        count_uglu = game.figure.transform.GetChild(0).transform.childCount;
        uglu = new GameObject[count_uglu];

        for (int i = 0; i < count_uglu; i++)
            uglu[i] = game.figure.transform.GetChild(0).transform.GetChild(i).gameObject;
    }
	
	// Update is called once per frame
	void Update () {
      
        mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = new Vector2(mousePos.x, mousePos.y);

        if (draw && On)
        {
            GameObject g = (GameObject)Instantiate(hwost);
            g.transform.position = new Vector3(transform.position.x, transform.position.y, 0);
        }
        else
        {
            point = 0;
            cur_ugol = -1;
            last_ugol = -1;
        }

        if (Input.GetMouseButtonDown(0)) On = true;
        if (Input.GetMouseButtonUp(0)) On = false;

        string poi="";
        for (int i = 0; i < point; i++)
            poi += "0";
        ugol_bar.text = poi;
	}

    void OnMouseDown()
    {
        On = true;
    }

    void OnMouseUp()
    {


        On = false;
        
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        
        if (col.name == "figure")
        {
            draw = true;
        }
        
        if (draw && On)
        {
            for (int i = 0; i < count_uglu; i++)
            {
              
                    if (col.name == uglu[i].name)
                    {
                        
                        if (last_ugol != i)
                        {
                            cur_ugol = i;



                            if (point > 1)
                            {

                                if (vector == 1)
                                {
                                    if (last_ugol == cur_ugol - 1 || (cur_ugol == 0 && last_ugol == (count_uglu - 1)))
                                    {

                                        last_ugol = cur_ugol;
                                       
                                      //  Debug.Log(cur_ugol);
                                        point++;
                                       
                                        if (point == count_uglu)
                                        {
                                            TheEnd();

                                        }
                                    }
                                    else
                                    {
                                        Debug.Log("-----------NOOOOO-------------");
                                        
                                        point = 0;
                                        last_ugol = -1;
                                        cur_ugol = -1;
                                    }
                                }
                                if (vector == -1)
                                {
                                    if (last_ugol == cur_ugol + 1 || (last_ugol == 0 && cur_ugol == (count_uglu - 1)))
                                    {

                                        last_ugol = cur_ugol;

                                     //   Debug.Log(cur_ugol);
                                        point++;
                                       
                                        if (point == count_uglu)
                                        {
                                            TheEnd();

                                        }
                                    }
                                    else
                                    {
                                        Debug.Log("-----------NOOOOO-------------");
                                        
                                        point = 0;
                                        last_ugol = -1;
                                        cur_ugol = -1;
                                    }
                                }

                            }
                            else
                                switch (point)
                                {
                                    case 0:
                                        {
                                            last_ugol = cur_ugol;
                                            point++;
                                            if (point == count_uglu)
                                            {
                                                TheEnd();

                                            }
                                        } break;
                                    case 1:
                                        {
                                            if (last_ugol == cur_ugol + 1 || (cur_ugol == count_uglu - 1 && last_ugol == 0))
                                                vector = -1;
                                            if (last_ugol == cur_ugol - 1 || (cur_ugol == 0 && last_ugol == count_uglu))
                                                vector = 1;

                                            last_ugol = cur_ugol;
                                            point++;
                                            if (point == count_uglu)
                                            {
                                                TheEnd();

                                            }
                                        } break;

                                }
                         //   Debug.Log("vector " + vector + " curUg: " + cur_ugol);
                        }
                        else 
                        {
                            Debug.Log("-----------NOOOOO-------------");
                            
                            point = 0;
                            last_ugol = -1;
                            cur_ugol = -1;
                        }
                }
               
            }
        }
    }

    private void TheEnd()
    {
       
        Debug.Log("++++++++++The End++++++++++++");
        point = 0;
       
        last_ugol = -1;
        cur_ugol = -1;
        uglu = new GameObject[3]; 
        game.Next();
        
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.name == "figure")
        {
            draw = false;
            point = 0;
            cur_ugol = -1;
            last_ugol = -1;
            
        }
    }
}
