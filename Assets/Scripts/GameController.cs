﻿using UnityEngine;
using System.Collections;
using System.IO;

public class GameController : MonoBehaviour
{

    public bool play = false;
    
    public GameObject [] figures;
    public Sprite[] images;
    public int curr_figure;
    public int count_fig;

    public TextMesh bar;
    public string str_bar = "000000000000000000000000000000";
    public float last_time, time, time_round = 30;
    public float interval = 1f;
    static string En = "/n";
    public string daWinchi_hello = "Здравствуй, ученик!";
    public string daWinchi_brifing = "Ты должен на скорость срисовывать мои картины.";
    public string daWinchi_go = "Для того что бы начать раунд жми СТАРТ!";
    public TextMesh say, point_component;
    public Animator DaWi;
    public int max_rez, rez;
    public string rez_path;
    public GameObject figure;
    public Draw risowach;
    public GameObject original;
    public Sprite mona;
    private int point;
    // Use this for initialization

 
    void Start()
    {
        rez_path = Application.dataPath + "/StreamingAssets/save.txt";
        original = GameObject.Find("original");
        figures = Resources.LoadAll<GameObject>("figures");
        Sprite [] tmp_ima = Resources.LoadAll<Sprite>("images");
        point_component = GameObject.Find("point").GetComponent<TextMesh>(); 

        risowach = GameObject.FindObjectOfType<Draw>();

        images = new Sprite[figures.Length];
        for (int i = 0; i < figures.Length; i++)
        {
            int index=0;
            while (figures[i].name != tmp_ima[index].name)
            {
                index++;
            }
            images[i] = tmp_ima[index];
        }

            time = time_round;
        bar = GameObject.Find("bar").GetComponent<TextMesh>();
        say = GameObject.Find("say").GetComponent<TextMesh>();
        DaWi = GameObject.Find("DaWinch").GetComponent<Animator>();

        daWinchi_brifing = daWinchi_brifing.Replace(" срисовывать мои картины", "\n  срисовывать мои картины");
        daWinchi_go = daWinchi_go.Replace("жми СТАРТ!", "\nжми СТАРТ!");


        figure = GameObject.Find("figure");
        curr_figure = Random.Range(0, figures.Length);
        new_figure();
        max_rez = int.Parse(File.ReadAllText(rez_path));
        original.GetComponent<SpriteRenderer>().sprite = mona;
    }

    private void new_figure()
    {
        if (figure.transform.childCount > 0)
        { 
          
            figure.transform.GetChild(0).gameObject.transform.position = new Vector3(100,100,5);
            figure.transform.GetChild(0).gameObject.transform.parent = null;
        }
       
        curr_figure = Random.Range(0, figures.Length);
        GameObject g = (GameObject)Instantiate(figures[curr_figure]);
        
        Debug.Log("Ready");
        g.name = figures[curr_figure].name;
        g.transform.position = figure.transform.position;
        g.transform.parent = figure.transform;

        original.GetComponent<SpriteRenderer>().sprite = images[curr_figure];
    }

    // Update is called once per frame
    void Update()
    {
        point_component.text = point.ToString();
        //MENU
        if (Time.time >= last_time + interval)
        {
            last_time = Time.time;

            switch ((int)last_time)
            {
                case 0: DaWi.Play("speak"); say.text = daWinchi_hello; break;
                case 3: DaWi.Play("speak"); say.text = daWinchi_brifing; break;
                case 5: DaWi.Play("speak"); say.text = daWinchi_go; break;
            }

            //GAME
            if (play)
            {

                time--;


               
                var str = bar.text;
                var c = str.ToCharArray();
                str = "";
                for (int i = 0; i < c.Length - 1; i++)
                    str += c[i];
                bar.text = str;

                switch (c.Length)
                {
                    case 15: bar.color = Color.yellow; break;
                    case 7: bar.color = Color.red; break;
                    
                }
                if (time == 0)
                {
                    original.GetComponent<SpriteRenderer>().sprite = mona;
                    play = false;
                    rez = point;
                    if (rez > max_rez)
                    {
                        max_rez = rez;
                        File.WriteAllText(rez_path, rez.ToString());
                    }
                   
                    point = 0;
                    interval = 1;
                    start_round();
                }
            }
        }
    }

    private void start_round()
    {
        
        bar.text = str_bar;
        time = time_round;
        bar.color = Color.green;
    }

    void OnGUI()
    {
        if (!play)
        {
            GUI.Box(new Rect(Screen.width / 2 - 80, Screen.height / 2 - 60, 160, 100), "Рекорд: " + max_rez + "\nТекуший результат " + rez);
            if (GUI.Button(new Rect(Screen.width / 2 - 50, Screen.height / 2 - 15, 100, 30), "Старт"))
            {
                risowach.set_figure();
                play = true;
                DaWi.Play("speak");
                original.GetComponent<SpriteRenderer>().sprite = images[curr_figure];
                say.text = "Рисуй!!!";
            }
        }

        if (GUI.Button(new Rect(Screen.width - 100, 30, 70, 40), "Выход")) Application.Quit();
    }

    public void Next()
    {
        interval -= 0.03f;
        point++;
        DaWi.Play("speak");
        say.text = "Превосходно!!!";
        
        Debug.Log("Delete");
        new_figure() ;
        start_round();

        risowach.set_figure();
        
    }

   
}
